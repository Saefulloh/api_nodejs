var db = require('../config/db_postgres');
const HttpStatus = require('http-status-codes');

function getList(req,res,next){
	db.any('select id,name,email,position,address from public.user')
	.then(function(data){
		res.status(200)
		.json({
			status: 'success',
			message: 'Retrieved List',
			data: data,
		});
	})
	.catch(function(err){
		return next(err);
	})
}

function getListOne(req, res, next) {
  var userid = parseInt(req.params.id);
  db.one('select id,name,email,position,address from public.user where id = $1', userid)
    .then(function (data) {
      res.status(200)
        .json({
          status: 'success',
          data: data,
          message: 'Retrieved ONE user'
        });
    })
    .catch(function (err) {
      //console.log(err);
      // return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ error: err, message: err.message }); // 500
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ message: err.message }); // 500
    });
}

function createUser(req, res, next) {
    db.none('insert into "user"(id, name, email)' +
      'values(${id}, ${name}, ${email})',
    req.body)
    .then(function (data) {
      res.status(200)
        .json({
          status: 'success',
          message: 'Inserted one user'
        });
    })
    .catch(function (err) {
      return next(err);
    });
}

function updateUser(req, res, next) {
  // console.log(req.body);
  db.none('update "user" set name=$1, email=$2 where id=$3',
    [req.body.name, req.body.email, parseInt(req.body.id)])
    .then(function () {
      res.status(200)
        .json({
          status: 'success',
          message: 'Updated User'
        });
    })
    .catch(function (err) {
      return next(err);
    });
}

function deleteUser(req, res, next) {
  var userid = parseInt(req.params.id);
  db.result('delete from "user" where id = $1', userid)
    .then(function (result) {
      /* jshint ignore:start */
      res.status(200)
        .json({
          status: 'success',
          message: `Removed ${result.rowCount} puppy`
        });
      /* jshint ignore:end */
    })
    .catch(function (err) {
      return next(err);
    });
}

module.exports = {
	getList: getList,
	getListOne: getListOne,
  createUser: createUser,
  updateUser: updateUser,
	deleteUser: deleteUser,
}