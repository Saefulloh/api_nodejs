const jwt = require('jsonwebtoken');
var db = require('../config/db_postgres');
const HttpStatus = require('http-status-codes');

function getList(req,res,next){
	db.any('select * from public.employee order by id desc')
	.then(function(data){
		res.status(200)
		.json({
			status: 'success',
			message: 'Retrieved List',
			data: data,
		});
	})
	.catch(function(err){
		return next(err);
	})
}

function getListOne(req, res, next) {
  var employeeid = parseInt(req.params.id);
  db.one('select * from public.employee where id = $1', employeeid)
    .then(function (data) {
      res.status(200)
        .json({
          status: 'success',
          data: data,
          message: 'Retrieved ONE employee'
        });
    })
    .catch(function (err) {
      //console.log(err);
      // return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ error: err, message: err.message }); // 500
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ message: err.message }); // 500
    });
}

function createEmployee(req, res, next) {
    db.none('insert into "employee"(name, position)' +
      'values(${name}, ${position})',
    req.body)
    .then(function (data) {
      res.status(200)
        .json({
          status: 'success',
          message: 'Inserted one employee'
        });
    })
    .catch(function (err) {
      return next(err);
    });
}

function updateEmployee(req, res, next) {
  // console.log(req.body);
  db.none('update "employee" set name=$1, position=$2 where id=$3',
    [req.body.name, req.body.position, parseInt(req.body.id)])
    .then(function () {
      res.status(200)
        .json({
          status: 'success',
          message: 'Updated one employee'
        });
    })
    .catch(function (err) {
      return next(err);
    });
}

function deleteEmployee(req, res, next) {
  var employeeid = parseInt(req.params.id);
  db.result('delete from "employee" where id = $1', employeeid)
    .then(function (result) {
      /* jshint ignore:start */
      res.status(200)
        .json({
          status: 'success',
          message: `Removed ${result.rowCount} employee`
        });
      /* jshint ignore:end */
    })
    .catch(function (err) {
      return next(err);
    });
}

module.exports = {
	getList: getList,
	getListOne: getListOne,
  	createEmployee: createEmployee,
 	updateEmployee: updateEmployee,
	deleteEmployee: deleteEmployee,
}