const jwt = require('jsonwebtoken');
var db = require('../config/db_postgres');
const HttpStatus = require('http-status-codes');

function getUser(req,res,next){
	var email = req.body.email;
	var password = req.body.password;
	// console.log(password);
	// console.log(email);
 	 db.one('select * from public.user where email = $1', email)
	.then(function(data){

		if(password == data.password){

			const user = {
				email: data.email,
				password: password
			}
			const token = jwt.sign({user}, 'secretkey', {expiresIn: '1h'});
			res.status(200)
			.json({
				status: 'success',
				message: 'Token Created',
				data: token,
			});
		} else {
			res.status(400)
			.json({
				status: 'wrong password'
			});
		}

	})
	.catch(function(err){
		return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ message: err.message }); // 500
	})
}


module.exports = {
	getUser: getUser
}