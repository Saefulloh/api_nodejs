var express = require('express');
var router = express.Router();
var users = require('../models/users');
var checkAuth = require('../middleware/check_auth');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/api/user', checkAuth, users.getList);
router.get('/api/user/:id', checkAuth, users.getListOne);
router.post('/api/createUser', checkAuth, users.createUser);
router.put('/api/updateUser', checkAuth, users.updateUser);
router.delete('/api/deleteUser/:id', checkAuth, users.deleteUser);

module.exports = router;
