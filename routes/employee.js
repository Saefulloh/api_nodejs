var express = require('express');
var router = express.Router();
var employee = require('../models/employee');
var checkAuth = require('../middleware/check_auth');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/api/employee', checkAuth, employee.getList);
router.get('/api/employee/:id', checkAuth, employee.getListOne);
router.post('/api/createEmployee', checkAuth, employee.createEmployee);
router.put('/api/updateEmployee', checkAuth, employee.updateEmployee);
router.delete('/api/deleteEmployee/:id', checkAuth, employee.deleteEmployee);

module.exports = router;
