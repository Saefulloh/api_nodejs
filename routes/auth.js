var express = require('express');
var router = express.Router();
const jwt = require('jsonwebtoken');
const auth = require('../models/auth');
var checkAuth = require('../middleware/check_auth');

/* GET users listing. */
router.post('/', (req, res) => {
	const user = {
		username: req.body.username,
		password:  req.body.password
	}
	console.log(user);
	jwt.sign({user}, 'secretkey', {expiresIn: '30m'}, (err,token) => {
		res.json({
			token
		});
	});
});

router.post('/login', auth.getUser);


module.exports = router;
