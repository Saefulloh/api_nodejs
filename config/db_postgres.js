const promise = require('bluebird');

const options = {
  // Initialization Options
  promiseLib: promise
};

const pgp = require('pg-promise')(options);
const connectionString = 'postgres://postgres:password@127.0.0.1:5432/dbpostgres';
const db = pgp(connectionString);

// if(db){
// 	console.log('Connnected');
// }

module.exports = db;

